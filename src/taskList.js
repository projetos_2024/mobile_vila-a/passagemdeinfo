import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Vai Sao Paulo",
      date: "2024-02-27",
      time: "4:30",
      address: "Copa Brasil",
    },
    {
      id: 2,
      title: "Felipe Vilaça",
      date: "2024-03-05",
      time: "10:00",
      address: "Lindo ",
    },
    {
      id: 3,
      title: "E os D",
      date: "2024-03-10",
      time: "15:30",
      address: "244",
    },
  ];
  const taskPress = (task) => {
    navigation.navigate("DetalhesDasTarefas", { task });
  };

  return (
    <View>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default TaskList;
