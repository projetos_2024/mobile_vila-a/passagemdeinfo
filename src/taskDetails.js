import React from "react";
import {View,Text,TouchableOpacity, FlatList} from "react-native";

const TaskDetails = ({route})=>{
    const {task} = route.params;
    
    
    return(
        <View>
            <Text>Detalhes da Tarefas:</Text>
            <Text>data: {task.date}</Text>
            <Text>Hora: {task.time}</Text>
            <Text>Local: {task.address}</Text>
        </View>
    )


}
export default TaskDetails;